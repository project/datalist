<?php

namespace Drupal\datalist\Plugin\WebformElement;

use Drupal\Core\Form\FormStateInterface;
use Drupal\webform\Plugin\WebformElementBase;
use Drupal\webform\WebformSubmissionInterface;
use Drupal\datalist\Element\Datalist as DatalistElement;

/**
 * Provides a 'datalist' element.
 *
 * @WebformElement(
 *   id = "webform_datalist",
 *   label = @Translation("Datalist"),
 *   description = @Translation("Provides a webform datalist element."),
 *   category = @Translation("Options elements"),
 * )
 *
 * @see \Drupal\webform_example_element\Element\WebformExampleElement
 * @see \Drupal\webform\Plugin\WebformElementBase
 * @see \Drupal\webform\Plugin\WebformElementInterface
 * @see \Drupal\webform\Annotation\WebformElement
 */
class Datalist extends WebformElementBase {

  /**
   * {@inheritdoc}
   */
  protected function defineDefaultProperties() {
    // Here you define your webform element's default properties,
    // which can be inherited.
    //
    // @see \Drupal\webform\Plugin\WebformElementBase::defaultProperties
    // @see \Drupal\webform\Plugin\WebformElementBase::defaultBaseProperties
    return [
        'multiple' => '',
        'size' => '',
        'minlength' => '',
        'maxlength' => '',
        'placeholder' => '',
        'options' => [],
        'use_keys' => FALSE,
        'clear_button' => DatalistElement::getClearButton(),
        'down_button' => DatalistElement::getDownButton(),
        'clear_button_description' => DatalistElement::getClearButtonDescription(),
        'autocomplete_route_name' => '',
        'autocomplete_route_parameters' => '',
      ] + parent::defineDefaultProperties();
  }

  /* ************************************************************************ */

  /**
   * {@inheritdoc}
   */
  public function prepare(array &$element, WebformSubmissionInterface $webform_submission = NULL) {
    parent::prepare($element, $webform_submission);

    // Here you can customize the webform element's properties.
    // You can also customize the form/render element's properties via the
    // FormElement.
    //
    // @see \Drupal\webform_example_element\Element\WebformExampleElement::processWebformElementExample
    $element['#type'] = 'datalist';
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);
    // Here you can define and alter a webform element's properties UI.
    // Form element property visibility and default values are defined via
    // ::defaultProperties.
    //
    // @see \Drupal\webform\Plugin\WebformElementBase::form
    // @see \Drupal\webform\Plugin\WebformElement\TextBase::form
    // Create a custom field set for the example element.

    $form['datalist'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Datalist options'),
      '#open' => TRUE,
    ];
    $form['datalist']['options'] = [
      '#type' => 'webform_element_options',
      '#title' => $this->t('Options'),
      '#required' => TRUE,
    ];
    $form['datalist']['down_button'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Down button value'),
      '#default_value' => DatalistElement::getDownButton(),
    ];
    $form['datalist']['clear_button'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Clear button'),
      '#default_value' => DatalistElement::getClearButton(),
    ];
    $form['datalist']['clear_button_description'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Clear button description'),
      '#default_value' => DatalistElement::getClearButtonDescription(),
    ];
    $form['datalist']['use_keys'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Use keys'),
      '#description' => $this->t('When selected the key, when you have selected an element, will be visible to the end user otherwise the label which is nicer.'),
      '#default_value' => FALSE,
    ];
    $form['datalist']['autocomplete_route_name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Autocomplete route name'),
      '#description' => $this->t('Datalist native element doesnt work in certain mobile browsers, thats why there is a fallback foreseen if needed.'),
      '#default_value' => '',
    ];
    $form['datalist']['autocomplete_route_parameters'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Autocomplete route parameters'),
      '#description' => $this->t('Enter as key:value comma separated.'),
      '#default_value' => '',
    ];

    return $form;
  }

}
