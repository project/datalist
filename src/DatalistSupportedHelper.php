<?php

namespace Drupal\datalist;

/**
 * @file
 * Helper module.
 */

/**
 * Just helper functions.
 */
class DatalistSupportedHelper {

  /**
   * Checks if the current browser(user-agent) is supported.
   *
   * @return bool
   *   Yes supported, no not so falling back if allowed in the settings.
   */
  public static function isSupportedBrowser() {
    $userAgent = $_SERVER['HTTP_USER_AGENT'];

    $isFirefoxMobileAndroid = (str_contains($userAgent, 'Mozilla/5.0 (Android')
      && str_contains($userAgent, 'Firefox/')
      && str_contains($userAgent, 'Gecko/'));
    $isFirefoxMobileIOS = (str_contains($userAgent, 'Mozilla/5.0 (iP')
      && str_contains($userAgent, 'FxiOS/1')
      && str_contains($userAgent, 'AppleWebKit/'));

    $isEdgeMobileAndroid = (str_contains($userAgent, 'Mozilla/5.0 (Android')
      && str_contains($userAgent, 'AppleWebKit/')
      && str_contains($userAgent, 'Edge/'));
    $isEdgeMobileIOS = (str_contains($userAgent, 'Mozilla/5.0 (Android')
      && str_contains($userAgent, 'AppleWebKit/')
      && str_contains($userAgent, 'EdgiOS/'));

    return !$isEdgeMobileAndroid
      && !$isEdgeMobileIOS
      && !$isFirefoxMobileAndroid
      && !$isFirefoxMobileIOS;
  }

}
