<?php

namespace Drupal\datalist\Cache\Context;

use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Cache\Context\CacheContextInterface;
use Drupal\datalist\DatalistSupportedHelper;

/**
 * Defines the 'datalist supported' cache context.
 *
 * Cache context ID: 'datalist_supported'.
 */
class DatalistSupportedCacheContext implements CacheContextInterface {

  /**
   * {@inheritdoc}
   */
  public static function getLabel() {
    return 'Datalist supported';
  }

  /**
   * {@inheritdoc}
   */
  public function getContext() {
    return DatalistSupportedHelper::isSupportedBrowser();
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheableMetadata() {
    return new CacheableMetadata();
  }

}
