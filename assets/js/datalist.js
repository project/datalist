/**
 * @file
 * Datalist clear button handling.
 */
(function (once) {
  'use strict';

  /**
   * Drupal behavior.
   */
  Drupal.behaviors.datalist = {
    attach: function (context) {

      let datalistsFormElements = once('datalist', '.form-type--datalist', context);

      if (datalistsFormElements.length === 0) {
        return;
      }

      // Handle unsupported browsers via JS.
      const isBrowserSupported = Drupal.behaviors.datalist.browserSupported();

      datalistsFormElements.forEach(element => {

        let form = element.closest('form');
        let input = element.querySelector('input');
        let clear = element.querySelector('.clear');
        let datalist = element.querySelector('datalist');

        // Update the initial state of the clear button.
        Drupal.behaviors.datalist.updateInputClearButton(input);

        // Chrome fills in input later on when going back.
        input.addEventListener('focus', function (e) {
          Drupal.behaviors.datalist.updateInputClearButton(e.target);
        });

        form.addEventListener('submit', function (e) {
          Drupal.behaviors.datalist.selectFirstResult(input, datalist);
        });
        input.addEventListener('blur', function (e) {
          Drupal.behaviors.datalist.selectFirstResult(e.target, datalist);
        });

        input.addEventListener('input', function (e) {
          Drupal.behaviors.datalist.updateInputClearButton(e.target);
        });

        clear.addEventListener('click', function (e) {
          e.preventDefault();
          Drupal.behaviors.datalist.clearInput(e.currentTarget);
        });

        if (isBrowserSupported) {
          Drupal.behaviors.datalist.removeAutocomplete(input);
        } else {
          Drupal.behaviors.datalist.removeDataList(input, datalist);
        }

      });
    },
    updateInputClearButton: function (input) {
      let clear = input.parentElement.querySelector('.clear');

      if (!input.value || input.value === '') {
        clear.classList.add('not-visible');
        clear.classList.remove('visible');
        clear.setAttribute('tabIndex', '-1');
      } else {
        if (!clear.classList.contains('visible')) {
          clear.classList.add('visible');
          clear.classList.remove('not-visible');
          clear.setAttribute('tabIndex', '0');
        }
      }

    },
    clearInput: function (clear) {
      let input = clear.parentElement.querySelector('input');

      input.value = '';
      input.dispatchEvent(new Event('input', { bubbles: true }));
      input.dispatchEvent(new Event('change', { bubbles: true }));
      input.focus();
    },
    removeDataList: function (input, datalist) {
      input.removeAttribute('list');
      datalist.outerHTML = '';
      input.parentElement.querySelector('.datalist__down-button').outerHTML = '';
    },
    removeAutocomplete: function (input) {
      input.classList.remove('form-autocomplete');
      input.classList.remove('ui-autocomplete-input');
      input.removeAttribute('data-autocomplete-path');
    },
    browserSupported: function () {
      const userAgent = window.navigator.userAgent;

      const isFirefoxMobileAndroid = (userAgent.includes('Mozilla/5.0 (Android')
        && userAgent.includes('Firefox/')
        && userAgent.includes('Gecko/'));
      const isFirefoxMobileIOS = (userAgent.includes('Mozilla/5.0 (iP')
        && userAgent.includes('FxiOS/1')
        && userAgent.includes('AppleWebKit/'));
      const isEdgeMobileAndroid = (userAgent.includes('Mozilla/5.0 (Android')
        && userAgent.includes('AppleWebKit/')
        && userAgent.includes('Edge/'));
      const isEdgeMobileIOS = (userAgent.includes('Mozilla/5.0 (Android')
        && userAgent.includes('AppleWebKit/')
        && userAgent.includes('EdgiOS/'));

      return !isEdgeMobileAndroid
        && !isEdgeMobileIOS
        && !isFirefoxMobileAndroid
        && !isFirefoxMobileIOS;
    },
    selectFirstResult: function (input, datalist) {
      if (input.value.length === 0) {
        return;
      }

      const options = Array.from(datalist.options).map(function (el) {
        return el.value;
      });
      const relevantOptions = options.filter(function (option) {
        return option.toLowerCase().includes(input.value.toLowerCase());
      });


      // Special use case where relevant options contains:
      // Klaprozenlaan & rozenlaan.
      // If you select rozenlaan then it shouldt select the first value.
      if (relevantOptions.includes(input.value)) {
        return;
      }

      if (relevantOptions.length > 0) {
        input.value = relevantOptions.shift();
        Drupal.announce(Drupal.t('Invalid value. Changed value to first filtered value based upon your input. New value is: %value', { '%value': input.value }));
      }
    },
  };

})(once);
